import * as Users from './users.controllers';
import * as Admin from './admin.controllers';

const RootController = {
  Users,
  Admin
};

export default RootController;