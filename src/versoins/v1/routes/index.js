import UserRouter from './users.routes';
import AdminRouter from './admin.routes';

const RootRouter = {
  UserRouter,
  AdminRouter
}

export default RootRouter;